#pragma once
#include "dialogue.h"
#include <stack>
class dialogueiterator
{
protected:
	PhraseNode* node;
	map<string, PhraseNode*> collection;
	map<string, PhraseNode*>::iterator it;
public:
	virtual PhraseNode* _next() = 0;
	virtual PhraseNode* _this() = 0;
};


class lr_iterator : public dialogueiterator
{
public:
	lr_iterator(PhraseNode* n) { this->node = n; cnts.push(0); };

	stack<int> cnts;
	PhraseNode* _next()
	{
		if (!this->node->children.empty())
		{
			if (this->cnts.top() < this->node->children.size())
			{
				this->node = std::next(this->node->children.begin(), cnts.top())->second;
				int top = cnts.top(); cnts.pop(); cnts.push(top + 1);
				cnts.push(0);
				return this->node;
			}
		}
		if (this->node->parent != nullptr)
		{
			cnts.pop();

			this->node = this->node->parent;
			return this->_next();
		}
		else return nullptr;
	}
	PhraseNode* _this()
	{
		return this->node;
	}
	friend class Dialogue;
};
