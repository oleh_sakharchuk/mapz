#pragma once
#include <list>
#include <map>
#include <string>
using namespace std;

class PhraseNode
{
protected:
	string phrase;
	PhraseNode* parent = nullptr;
	map<string, PhraseNode*> children;

public:
	PhraseNode(string v, PhraseNode* p) { phrase = v; this->parent = p; }
	PhraseNode(const string&, const list<string> &);
	string getPhrase();
	map<string, PhraseNode*> getChildren();
	PhraseNode* getParent();
	friend class DialogueParser;
	friend class TreeWriter;
	friend class Dialogue;
	friend class lr_iterator;

};

