#include <iostream>
using namespace std;
#include "configuration.h"
#include "gameprocess.h"

class A
{
public:
	string s;
};


class A1 : public A
{
public:
	A1() { this->s = "a1"; }
};

class A2 : public A
{
public:
	A2() { this->s = "a2"; }
};

int main()
{
	A* ptr;
	ptr = new A1;
	A1* ptr1 = new A1;
	cout << ptr->s;
	return 0;
}