#include "plot.h"

Plot::Plot(const string& folder) : PlotPartDecorator(folder)
{
	this->level = this->subparts.begin();
}

void Plot::nextLevel()
{
	if (next(this->level) != this->subparts.end())
	{
		advance(this->level, 1);
	}
	else throw exception("end of game");
}

const string& Plot::getLevel() const
{
	return *this->level;
}
