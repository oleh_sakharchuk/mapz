#pragma once
#include <list>
using namespace std;

class Thing;

class Hero
{
private:
	int totalPoints;
	int freePoints;
	int smartness;
	int strength;
	int charisma;
	list<Thing> things;
	friend class HeroMediator;
};

