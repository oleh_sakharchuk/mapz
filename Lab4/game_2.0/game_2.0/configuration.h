#pragma once
#include <string>
#include "hero.h"
#include "heromediator.h"
using namespace std;
class Plot {};
class Level {};
class Dialogue {};
class PhraseNode {};

class Configuration
{
private:
	Hero hero;
	Plot plt;
	Level lvl;
	PhraseNode phnd;
public:
	//Configuration(const Configuration&);
	//Configuration(const Character&, const Plot&, const Level&, const PhraseNode&);
	Configuration();
	void set(const Configuration&);
	void set(const Hero&, const Plot&, const Level&, const PhraseNode&);
	void setHero();
	const Hero& getCharacter() const;
	const Plot& getPlot() const;
	const Level& getLevel() const;
	const PhraseNode& getNode() const;
};

