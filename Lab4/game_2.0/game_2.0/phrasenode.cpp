#include "phrasenode.h"

PhraseNode::PhraseNode(const string& title, const list<string>& variants)
{
	this->phrase = title;
	for (auto it = variants.begin(); it != variants.end(); it++)
	{
		this->children.insert({ *it, nullptr });
	}
}

string PhraseNode::getPhrase()
{
	return this->phrase;
}

map<string, PhraseNode*> PhraseNode::getChildren()
{
	return this->children;
}

PhraseNode* PhraseNode::getParent()
{
	return this->parent;
}
