#include "plotpart.h"

PlotPart::PlotPart()
{
}

PlotPart::PlotPart(const string& fold)
{
	this->path = fold;
	ifstream fin(fold + "plot.txt");
	if (fin.is_open())
	{
		string line;
		while (true)
		{
			getline(fin, line);
			if (fin.eof()) break;
			this->subparts.push_back(line);
		}
	}
}

const list<string>& PlotPart::getSubparts() const
{
	return this->subparts;
}
