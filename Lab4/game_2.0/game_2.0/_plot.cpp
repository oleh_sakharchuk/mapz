#include "_plot.h"

_plot::_plot(const string& fold)
{
	this->path = fold;
	ifstream fin(fold + "plot.txt");
	if (fin.is_open())
	{
		string line;
		while (true)
		{
			getline(fin, line);
			if (fin.eof()) break;
			this->levels.push_back(line);
		}
	}
}
