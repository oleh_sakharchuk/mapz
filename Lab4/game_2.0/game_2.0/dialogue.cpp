#include "dialogue.h"

PhraseNode* Dialogue::getNode()
{
	return this->startNode;
}

string Dialogue::repr()
{
	return this->detour(this->startNode, 0);
}

dialogueiterator* Dialogue::iterator(const string& tp)
{
	if (tp == "lr")
	{
		return new lr_iterator(this->startNode);
	}
	else throw exception("no available iterator with such name");
}

string Dialogue::detour(PhraseNode* node, const int depth)
{
	string str, spaces;
	for (int i = 0; i < depth; i++)
		spaces += ' ';
	str += spaces + node->getPhrase() + "\n";
	for (auto it = node->children.begin(); it != node->children.end(); it++)
	{
		str += spaces + it->first + "\n";
		str += this->detour(it->second, depth + 3);
	}
	return str;
}
