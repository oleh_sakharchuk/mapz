#include "gameprocess.h"

GameProcess* GameProcess::gp = nullptr;

GameProcess* GameProcess::getGP()
{
	if (GameProcess::gp == nullptr)
		GameProcess::gp = new GameProcess;
	else return GameProcess::gp;
}

void GameProcess::init(Configuration* pconfig)
{
	this->configs.push(pconfig);
	this->config = this->configs.top();
}

void GameProcess::back()
{
	if (this->configs.size() > 1)
	{
		this->configs.pop();
		this->config = this->configs.top();
	}
}

