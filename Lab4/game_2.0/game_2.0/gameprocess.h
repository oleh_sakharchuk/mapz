#pragma once
#include <exception>
#include <stack>
using namespace std;
#include "configuration.h"

class GameProcess
{
private:
	static GameProcess* gp;
	stack<Configuration*> configs;
	Configuration* config;
public:
	//get n set
	static GameProcess* getGP();
	void init(Configuration*);
	void back();
	// TO DO:
	//moments 
	// ADD CONROLLERS FOR CHARACTER AND PLOT INTERACTION
};

