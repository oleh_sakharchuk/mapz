#pragma once
#include "hero.h"

class HeroMediator
{
private:
	Hero* hero = nullptr;
public:
	void takeHero(Hero*);
	void upPoints(const int&);
	void upSmartness(const int&);
	void upStrength(const int&);
	void upCharisma(const int&);
};

