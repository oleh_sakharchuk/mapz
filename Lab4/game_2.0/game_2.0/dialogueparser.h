#pragma once
#include <fstream>
#include "dialogue.h"
#include "phrasenode.h"
using namespace std;

class DialogueParser
{
private:
	ifstream fin;
	string read();
	static DialogueParser* parser;
public:
	static DialogueParser& getParser();
	PhraseNode* parseTree(int, PhraseNode*);
	list<Dialogue*> parseDialogues(const string&);
};

int spaceCounter(const string&);
string rstrip(const string&);