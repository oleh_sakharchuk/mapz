#pragma once
#include "phrasenode.h"
#include "dialogueiterator.h"

class dialogueiterator;
class Dialogue
{
protected:
	string title;
	PhraseNode* startNode;
	string detour(PhraseNode*, const int);
public:
	Dialogue() {};
	PhraseNode* getNode();
	string repr();
	dialogueiterator* iterator(const string&);
	friend class DialogueParser;
	friend class lr_iterator;

};

