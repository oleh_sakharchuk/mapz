#pragma once
class Character
{
private:
	static Character* currentCharacter;
public:
	int muscle = 0;
	int smartness = 0;
	int nimbleness = 0;
	int totalPoints;
	Character();
	static Character* getInstance();
	void upMuscle(const int&);
	void upSmartness(const int&);
	void upNimbleness(const int&);
};