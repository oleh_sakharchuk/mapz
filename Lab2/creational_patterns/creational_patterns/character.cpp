#include "character.h"
Character* Character::currentCharacter = nullptr;
Character::Character()
{
	this->totalPoints = 3;
}
Character* Character::getInstance()
{
	if (Character::currentCharacter == nullptr)
	{
		Character::currentCharacter = new Character();
	}
	return Character::currentCharacter;
}
void Character::upMuscle(const int& val)
{
	this->muscle = val;
}
void Character::upSmartness(const int& val)
{
	this->smartness = val;
}
void Character::upNimbleness(const int& val)
{
	this->nimbleness = val;
}