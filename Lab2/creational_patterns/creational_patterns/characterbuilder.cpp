#include "characterbuilder.h"
void CharacterBuilder::setCharacter()
{
	this->character = Character::getInstance();
}
Character* CharacterBuilder::getCharacter()
{
	return this->character;
}
void CharacterBuilder::customBuild(const int& _muscle, const int& _smartness, const int&
	_nimleness)
{
	if (_muscle + _smartness + _nimleness < this->character->totalPoints)
	{
		this->character->muscle = _muscle;
		this->character->smartness = _smartness;
		this->character->nimbleness = _nimleness;
	}
}
void CharacterBuilder::setCharacter()
{
	this->character = Character::getInstance();
}
Character* CharacterBuilder::getCharacter()
{
	return this->character;
}
void CharacterBuilder::customBuild(const int& _muscle, const int& _smartness, const int&
	_nimleness)
{
	if (_muscle + _smartness + _nimleness < this->character->totalPoints)
	{
		this->character->muscle = _muscle;
		this->character->smartness = _smartness;
		this->character->nimbleness = _nimleness;
	}
}
class SmartCB : public CharacterBuilder
{
public:
	void setMuscle() override;
	void setSmartness() override;
	void setNimbleness() override;
};