#include "creator.h"
void Creator::setBuilder(CharacterBuilder* _builder)
{
	this->builder = _builder;
}
Character* Creator::getCharacter()
{
	return this->builder->getCharacter();
}
void Creator::init()
{
	builder->setCharacter();
	builder->setMuscle();
	builder->setSmartness();
	builder->setNimbleness();
}