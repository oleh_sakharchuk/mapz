#pragma once
#include "characterbuilder.h"
class Creator
{
private:
	CharacterBuilder* builder;
public:
	void setBuilder(CharacterBuilder*);
	Character* getCharacter();
	void init();
};
