#pragma once
#include "character.h"

class CharacterBuilder
{
protected:
	Character* character;
public:
	void setCharacter();
	Character* getCharacter();
	virtual void setMuscle() = 0;
	virtual void setSmartness() = 0;
	virtual void setNimbleness() = 0;
	void customBuild(const int& _muscle, const int& _smartness, const int&
		_nimleness);
};