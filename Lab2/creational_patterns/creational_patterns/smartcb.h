#pragma once
#include "characterbuilder.h"

class SmartCB : public CharacterBuilder
{
public:
	void setMuscle() override;
	void setSmartness() override;
	void setNimbleness() override;
};
