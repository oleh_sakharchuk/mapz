#pragma once
#include "gamesetter.h"

class SavedGameSetter : protected GameSetter
{
public:
	GameProcess* setSaved(const string&);
};
GameProcess* SavedGameSetter::setSaved(const string& saveName)
{
	Configurator conf;
	Save save = conf.getSave(saveName);
	return this->setGame(save.getPlotName(), save.getCharacter());
}
