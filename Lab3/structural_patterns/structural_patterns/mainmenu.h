#pragma once
#include "gameprocess.h"

class MainMenu
{
	static MainMenu* menu;
	MainMenu();
public:
	static MainMenu& getMenu();
	GameProcess* startNewGame();
	GameProcess* startSavedGame();
	static void _exit();
};
