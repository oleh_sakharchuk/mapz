#pragma once
class GameSetter
{
protected:
	GameProcess* setGame(const string&, Character*);
public:
	GameProcess* setNew(const string&, const string&, const string&);
};
GameProcess* GameSetter::setGame(const string& plotName, Character* character)
{
	Configurator conf;
	return new GameProcess(character, conf.getPlot(plotName));
}
GameProcess* GameSetter::setNew(const string& plotName, const string& charType, const string&
	charName)
{
	Character* ch;
	if (charType == "botan")
	{
		ch = new Botan(charName);
	}
	else if (charType == "kachok")
	{
		ch = new Kachok(charName);
	}
	else if (charType == "torchok")
	{
		ch = new Torchok(charName);
	}
	else
	{
		ch = new Character(charName);
	}
	return this->setGame(plotName, ch);
}
