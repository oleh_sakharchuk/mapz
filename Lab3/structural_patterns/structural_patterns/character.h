#pragma once
#include <string>
#include <list>

using namespace std;

class Character
{
protected:
	int totalPoints;
	int freePoints;
	string name;
	int smartness;
	int strength;
	int energy;
	list<Thing> things;
	Character();
public:
	Character(const string&);
	void addPoints(const int&);
	void upSmartness(const int&);
	void upStrength(const int&);
	void upEnergy(const int&);
	void downSmartness(const int&);
	void downStrength(const int&);
	void downEnergy(const int&);
	const int& getSmartness() const;
	const int& getSthrength() const;
	const int& getEnergy() const;
	const string& getName();
};