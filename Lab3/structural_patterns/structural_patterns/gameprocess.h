#pragma once
#include <string>
#include <list>
using namespace std;

class GameProcess
{
private:
	Plot* plot;
	Character* character;
public:
	void enterSituation();
	void enterDialogue(const string&);
	list<string> reply(const string&);
	void interactWithThing(const string&);
	void upCharacterPoints(const int&);
	void upCharacterStrength(const int&);
	void upCharacterSmartness(const int&);
	void upCharacterEnergy(const int&);
	GameProcess(Character*, Plot*);
	~GameProcess();
};
