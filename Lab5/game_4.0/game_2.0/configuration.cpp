#include "configuration.h"

Configuration::Configuration(const Configuration& obj)
{
	this->hero = obj.hero;
	this->phnd = obj.phnd;

}

void Configuration::serialize(ofstream& out)
{
	this->hero.serialize(out);
	out << this->phnd->getPhrase();
}

void Configuration::deserialize(ifstream& fin)
{
	list<Dialogue*> dl = DialogueParser::getParser().parseDialogues("script.txt");
	auto df = *dl.begin();
	this->hero.deserialize(fin);
	string str;
	getline(fin, str);
	cout << endl << str << endl;
	auto it = df->getLRIt();
	while (true)
	{
		if (it._this()->operator*()->getPhrase() == str)
		{
			cout << "is" << endl;
			break;
		}
		cout << it._next()->operator*()->getPhrase() <<endl;
		it._next();
		cout << (*it)->getPhrase() << endl;
	}
	this->phnd = it._this()->operator*();
}

Configuration::Configuration(const Hero& hero, PhraseNode* nd)
{
	this->hero = hero;
	this->phnd = nd;
}

Hero* Configuration::getHero()
{
	return &this->hero;
}

PhraseNode* Configuration::getNode()
{
	return this->phnd;
}

Configuration& Configuration::operator=(const Configuration& cnf)
{
	if (this != &cnf)
	{
		this->hero = cnf.hero;
		this->phnd = cnf.phnd;
	}
	return *this;
}
