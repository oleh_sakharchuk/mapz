#pragma once
#include "phrasenode.h"
#include <iostream>
#include <fstream>
#include <stack>
class dialogueiterator
{
protected:
	PhraseNode* node;
	//map<string, PhraseNode*> collection;
	//map<string, PhraseNode*>::iterator it;
public:
	virtual dialogueiterator* _next() = 0;
	virtual dialogueiterator* _this() = 0;
	PhraseNode* operator *()
	{
		return this->node;
	}
};

class user_iterator : public dialogueiterator
{
private:
	dialogueiterator* _nxt(istream& str)
	{
		string key;
		getline(str, key);
		key = key.substr(0, key.length() - 2);
		if (this->node->children[key] != nullptr)
		{
			this->node->parent = this->node;
			this->node = this->node->children[key];
			return this;
		}
	}
public:
	user_iterator(PhraseNode* n) { this->node = n;  }
	dialogueiterator* _next()
	{
		return this->_nxt(std::cin);
	}
	dialogueiterator* _this() override { return this; }
	friend class GameProcess;
};

class lr_iterator : public dialogueiterator
{
	stack<int> cnts;
	dialogueiterator* _nxt()
	{
		if (!this->node->children.empty())
		{
			if (this->cnts.top() < this->node->children.size())
			{
				this->node = std::next(this->node->children.begin(), cnts.top())->second;
				int top = cnts.top(); cnts.pop(); cnts.push(top + 1);
				cnts.push(0);
				return this;
			}
		}
		if (this->node->parent != nullptr)
		{
			cnts.pop();

			this->node = this->node->parent;
			return this->_next();
		}
		else return nullptr;
	}
public:
	lr_iterator(PhraseNode* n) { this->node = n; cnts.push(0); };

	dialogueiterator* _next() override
	{
		return this->_nxt();
	}
	dialogueiterator* _this()
	{
		return this;
	}	
};
