#pragma once
#include <string>
#include <fstream>
#include "phrasenode.h"
#include "hero.h"
#include "heromediator.h"
#include "iserializable.h"
#include "dialogue.h"
#include "dialogueparser.h"
#include "dialogueiterator.h"
using namespace std;

class Configuration : public ISerializable
{
private:
	Hero hero;
	PhraseNode* phnd;
	
public:
	void serialize(ofstream&);
	void  deserialize(ifstream&);	
	Configuration() {};
	Configuration(const Hero&, PhraseNode*);
	Configuration(const Configuration&);
	Hero* getHero();
	PhraseNode* getNode();
	void setNode(PhraseNode* node) { this->phnd = node; }
	Configuration& operator = (const Configuration&);
	friend class GameProcess;
};

