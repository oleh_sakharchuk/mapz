﻿#include <iostream>
using namespace std;
#include "configuration.h"
#include "gameprocess.h"
#include "dialogue.h"
#include "dialogueparser.h"
#include "dialogueiterator.h"


int main()
{
	ofstream of;
	GameProcess* gp = GameProcess::getGP();
	gp->init();
	gp->play();
	return 0;
}