#pragma once
#include "hero.h"
//#include "gameprocess.h"

class Type 
{
public:
	void fight() { cout << "fight "; };
	void talk() { cout << "talk "; };
	void thinkof() { cout << "thinking "; };
};

class Strong : public Type
{
public:
	void fight() { Type::fight(); cout << "successful\n"; };
	void talk() { Type::talk(); cout << "unsuccessful\n"; };
	void thinkof(Hero*) { Type::thinkof(); cout << "unsuccessful\n"; };
};

class Smart : public Type
{
public:
	void fight() { Type::fight(); cout << "unsuccessful\n"; };
	void talk() { Type::talk(); cout << "unsuccessful\n"; };
	void thinkof() { Type::thinkof(); cout << "successful\n"; };
};

class Charismatic : public Type
{
public:
	void fight() { Type::fight(); cout << "unsuccessful\n"; };
	void talk() { Type::talk(); cout << "successful\n"; };
	void thinkof() { Type::thinkof(); cout << "sunuccessful\n"; };
};


class HeroMediator
{
private:
	Hero* hero = nullptr;
public:
	void takeHero(Hero*);
	void upPoints(const int&);
	void upSmartness(const int&);
	void upStrength(const int&);
	void upCharisma(const int&);
	void fight();
	void talk();
	void thinkof();
};

