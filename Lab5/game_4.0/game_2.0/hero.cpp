#include "hero.h"

void Hero::serialize(ofstream& out)
{
	
	out << this->name << endl << this->totalPoints << endl << this->freePoints << endl <<
		this->smartness << endl << this->strength << endl << this->charisma <<endl;
}

void Hero::deserialize(ifstream& fin)
{
	list<string> lines;
	string line;
	if (fin.is_open())
	{
		for (int i =0 ;i < 6; i++)
		{
			getline(fin, line);
			if (fin.eof()) break;
			lines.push_back(line);
			cout << line;
		}
		if (lines.size() == 6)
		{
			try
			{
				auto it = lines.begin();
				this->name = *it;
				//cout << hero->name;
				//cout << *next(it);
				this->totalPoints = stoi(*next(it, 1));
				this->freePoints = stoi(*next(it,2));
				this->smartness = stoi(*next(it, 3));
				this->strength = stoi(*next(it, 4));
				this->charisma = stoi(*next(it, 5));
			}
			catch (exception ex)
			{
				cout << ex.what();
			}
		}
	}
}

Hero::Hero(const string& name)
{
	this->name = name;
	this->freePoints = 0;
	this->smartness = 1;
	this->strength = 1;
	this->charisma = 1;
	this->totalPoints = 3;
}

Hero& Hero::operator=(const Hero& l)
{
	if (this != &l)
	{
		this->name = l.name;
		this->freePoints = l.freePoints;
		this->totalPoints = l.totalPoints;
		this->charisma = l.charisma;
		this->smartness = l.smartness;
		this->strength = l.strength;
		this->things = l.things;
	}
	return *this;
}

string Hero::repr()
{
	return to_string(this->freePoints);
}
