#pragma once
#include <string>
using namespace std;

class GameProcessMediator
{
public:
	virtual string notify(const string&) = 0;
};

