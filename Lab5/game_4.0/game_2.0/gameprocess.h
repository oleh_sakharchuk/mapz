#pragma once
#include <exception>
#include <stack>
using namespace std;
#include "configuration.h"
#include "dialogueiterator.h"
#include "heromediator.h"


class GameProcess
{
private:
	static GameProcess* gp;
	stack<Configuration> configs;
	Configuration config;
	user_iterator* it;
	HeroMediator hm;
	void promisciousSave();
	GameProcess() {};
public:
	//get n set
	static GameProcess* getGP();
	void init();
	void play();
	void back();
	void save();
	// TO DO:
	//moments 
	// ADD CONROLLERS FOR CHARACTER AND PLOT INTERACTION
};

