#pragma once
#include <list>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#include "iserializable.h"

class Thing {};
class Hero : public ISerializable
{
private:
	string name;
	int totalPoints;
	int freePoints;
	int smartness;
	int strength;
	int charisma;
	void serialize(ofstream&) override;
	void deserialize(ifstream&) override;
	list<Thing> things;
public:
	Hero() {};
	Hero(const string&);
	Hero& operator = (const Hero&);
	string repr();
	friend class HeroMediator;
	friend class OppMediator;
	friend class Configuration;
	friend class Type;
};

