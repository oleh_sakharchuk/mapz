#include "heromediator.h"

void HeroMediator::takeHero(Hero* h)
{
	this->hero = h;
}
#include <iostream>
void HeroMediator::upPoints(const int& p)
{
	this->hero->freePoints += p;
	this->hero->totalPoints += p;
	std::cout << "upPoints : points upped to " << this->hero->freePoints <<endl;
}

void HeroMediator::upSmartness(const int& smp)
{
	if (smp <= this->hero->freePoints)
	{
		this->hero->smartness += smp;
		this->hero->freePoints -= smp;
	}
}

void HeroMediator::upStrength(const int& stp)
{
	if (stp <= this->hero->freePoints)
	{
		this->hero->strength += stp;
		this->hero->freePoints -= stp;
	}
}

void HeroMediator::upCharisma(const int& chp)
{
	if (chp <= this->hero->freePoints)
	{
		this->hero->charisma += chp;
		this->hero->freePoints -= chp;
	}
}

void HeroMediator::fight()
{
	//this->type->fight();
}

void HeroMediator::talk()
{
	//this->type->talk();
}

void HeroMediator::thinkof()
{
	//this->type->thinkof();
}
