#pragma once
#include <iostream>
using namespace std;

__interface ISerializable
{
public:
	virtual void serialize(ofstream&) = 0;
	virtual void deserialize(ifstream&) = 0;

};

